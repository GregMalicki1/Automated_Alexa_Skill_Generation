# Automated_Alexa_Skill_Generation

## Problem Statement : 
Alexa skills are growing in popularity but requires the creater to be familiar with many technical details, such as writing software and connecting to various AWS components. This technical aspect is time consuming and holds many people back from trying, could this process be simplified so everyone could create their very own skill?

## Description : 
This repository contains software written for a semester long project in the college course *Intro to Software Engineering*. Our project simplifies the creation of an Alexa skill in a creative way. Creators no longer need to understand how to write software or how to connect to Amazon's AWS lambda or databases.

## Developer Guide :  
**Requirements :** 
Install Node and Git
-**Node.js** : https://nodejs.org/en/
-**Git**: https://git-scm.com/downloads 

1. Open the git command prompt and navigate to a folder to store this project in (Ex: desktop/my_projects)
2. Clone this repo using **git clone https://github.com/gvwarrior/Automated_Alexa_Skill_Generation.git**
3. Now type **cd Automated_Alexa_Skill_Generation** and then **cd server** 
4. Now in the server folder, run the command **npm install** to install the node modules our server will need 
5. Go back to the root folder by typing **cd ..**
6. Next, **cd client** and **npm install** to install the node modules our front-end application will need
7. Success, now time to run the application!  
8. While still in the **client** folder, type **npm run serve**
9. It will print out near the end **"App running at"**, navigate to the **"Local:"** adress in your browser (Ex: http://localhost:8080/)

### Starting the Webserver ###
1. Open git and navigate to the server directory (i.e. **Alexa_Skill_Generator/server**)
2. Run the command **npm run dev**
3. You should see a message saying the server has started (i.e. "Listening on port 8000...")



