const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const morgan = require('morgan')

// Create express application
const app = express()

/* 
// Add Middleware to Express app
// This adds functionality to express from other libraries
*/
app.use(morgan('tiny'))
app.use(cors())
app.use(bodyParser.json())

app.get('/', (req, res) => {
    res.json({
        message: 'Hello Mars!'
    })
})

/*
// Searches for an environment variable named 'PORT' on Host Site
// Else, use arbitrary default number for development
*/
const port = process.env.PORT || 8000
app.listen(port, () => {
    console.log(`Listening on port ${port}...`)
})